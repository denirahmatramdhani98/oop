<?php
include "animal.php";
include "frog.php";
include "ape.php";

// Nomor 1
$sheep = new Animal("shaun");
echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false
echo "<br>";

// Nomor 2
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo "<br>";
?>